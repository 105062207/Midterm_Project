# Software Studio 2018 Spring Midterm Project
## Notice
* Replace all [xxxx] to your answer

## Topic
* [Topic Name]
* Key functions (add/delete)
    1. signup/signin/google account
    2. public room
    3. add friends(by uid) friend unique talk room
        3.1 histroy remaind
    4. RWD
* Other functions (add/delete)
    1. change user display name
    2. user own photo(using URL source)
    3. [xxx]
    4. [xxx]

## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Membership Mechanism|20%|N|
|GitLab Page|5%|N|
|Database|15%|N|
|RWD|15%|N|
|Topic Key Function|15%|N|

## Advanced Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Third-Party Sign In|2.5%|N|
|Chrome Notification|5%|N|
|Use CSS Animation|2.5%|N|
|Security Report|5%|N|
|Other functions|1~10%|N|

## Website Detail Description
有公開跟私人聊天室，公開聊天室中除了名字/頭像外不會有期他個人資料，
朋友聊天室中可以看到彼此的Uid,會依照說話得是否為當時的user決定投像放置位置，
使用者頭像會在右邊，朋友頭像會在左邊
## Security Report (Optional)
如果沒有登入會員就無法進入聊天室，朋友之間的聊天紀錄僅有雙方用戶能接觸
第三方無法得到兩人的聊天紀錄