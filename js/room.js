function init() {
    var user_email = '';
    var user_displayName = '';
    //var user_URL = firebase.auth().currentUser.photoURL;
    firebase.auth().onAuthStateChanged(function (user) {
        var menu = document.getElementById('dynamic-menu');
        var photo='';
        //document.getElementById('user_img').src = user.photoURL;
        // Check user login
        if (user) {
            document.getElementById('user_img').src = user.photoURL;
            user_email = user.email;
            console.log(user.displayName) ;
            console.log(user.photoURL);
            user_displayName = user.displayName;
            user_URL = user.photoURL;
            console.log(user_URL);
            menu.innerHTML = "<span class='dropdown-item'>" + user.email + "</span><span class='dropdown-item' id='profile-btn'>Profile</span><span class='dropdown-item' id='logout-btn'>Logout</span>";
            /// TODO 5: Complete logout button event
            ///         1. Add a listener to logout button 
            ///         2. Show alert when logout success or error (use "then & catch" syntex)
            var btnLogout = document.getElementById("logout-btn");
            btnLogout.addEventListener('click',function(){
                firebase.auth().signOut().then(function(){
                    alert("success");
                    document.location.href="index.html";
                    document.getElementById('user_img').src="img/RF-logo.png";
                }).catch(function(e){
                    alert("error");
                });
            });
            var btnProfile = document.getElementById("profile-btn");
            btnProfile.addEventListener('click',function(){
                document.location.href = "profile.html";
            });
        } else {
            // It won't show any post if not login
            menu.innerHTML = "<a class='dropdown-item' href='signin.html'>Login</a>";
            document.getElementById('post_list').innerHTML = "";
        }
    });

    post_btn = document.getElementById('post_btn');
    post_txt = document.getElementById('comment');
    add_btn = document.getElementById('add');
    txt_id = document.getElementById('friendID');
    
    add_btn.addEventListener('click',function(){
        if(txt_id.value!="")
        {
            var userNow = firebase.auth().currentUser;
            var UserId = firebase.database().ref('users');
            var friendExist=false;
            UserId.once("value")
            .then(function(snapshot) {
                var inputID =txt_id.value;
                console.log(txt_id.value);
                
                friendExist = snapshot.hasChild(inputID);
                console.log(friendExist);
                if(friendExist)
                {
                    var friends ="";
                    var friendsName="";
                    var existed ="";
                    firebase.database().ref('users/'+userNow.uid+'/friend').once('value', function(snapshot) {
                        snapshot.forEach(function(childSnapshot) {
                            var friendlist = childSnapshot.val();
                            console.log(friendlist.friendId);
                            if(friendlist.friendId==txt_id.value) existed = true;
                            console.log(existed);
                        });
                        console.log(existed);
                        if(existed == true){
                            alert("You already has the friend!")
                        }else {
                            firebase.database().ref('users/').once('value', function(snapshot) {
                                snapshot.forEach(function(childSnapshot) {
                                    friends = childSnapshot.val();
                                    if(friends.UserId==txt_id.value) 
                                    {
                                        friendsName = friends.displayName;
                                        console.log(friendsName);
                                    }
                                    var UserFriend = firebase.database().ref('/users/'+userNow.uid+'/friend/'+txt_id.value);
                                    UserFriend.set({
                                        friendId: txt_id.value,
                                        friendName:friendsName,
                                        history: ""
                                    })
                                    var friendFriend = firebase.database().ref('/users/'+txt_id.value+'/friend/'+userNow.uid);
                                    friendFriend.set({
                                        friendId:userNow.uid,
                                        friendName:userNow.displayName,
                                        history:""
                                    })
                                });
                            });        
                        }    
                    });
                }
                else{
                    alert("User Not Exist");
                }
    
            });
            
            
        }
        //txt_id.value="";
    });

    post_btn.addEventListener('click', function () {
        if (post_txt.value != "") {
            /// TODO 6: Push the post to database's "com_list" node
            ///         1. Get the reference of "com_list"
            ///         2. Push user email and post data
            ///         3. Clear text field
            var newpost = firebase.database().ref('/com_list');
            newpost.push({
                //email : user_email,
                displayName : user_displayName,
                data : post_txt.value,
                photoURL: user_URL,
            })
            post_txt.value="";
        }
    });

    // The html code for post
    var str_before_username = "<div class='my-3 p-3 bg-white rounded box-shadow'><div class='media text-muted pt-3'>";
    var mid_str = "<p class='media-body pb-3 mb-0 small lh-125 border-bottom border-gray'><strong class='d-block text-gray-dark'>";
    var str_after_content = "</p></div></div>\n";
    //var img = "<img src=photo class='mr-2 rounded' style='height:32px;width:32px'>"+

    var postsRef = firebase.database().ref('com_list');
    // List for store posts html
    var total_post = [];
    // Counter for checking history post update complete
    var first_count = 0;
    // Counter for checking when to update new post
    var second_count = 0;

    postsRef.once('value')
        .then(function (snapshot) {
            /// TODO 7: Get all history posts when the web page is loaded and add listener to update new post
            ///         1. Get all history post and push to a list (str_before_username + email + </strong> + data + str_after_content)
            ///         2. Join all post in list to html in once
            ///         4. Add listener for update the new post
            ///         5. Push new post's html to a list
            ///         6. Re-join all post in list to html when update
            ///
            ///         Hint: When history post count is less then new post count, update the new and refresh html
            snapshot.forEach(function(childSnapshot){
                var childData = childSnapshot.val();
                console.log("childData:"+childData.displayName);
                total_post[total_post.length] = str_before_username+"<img src="+childData.photoURL+" alt='' class='mr-2 rounded' style='height:32px; width:32px;'>"+mid_str+childData.displayName+"</strong>"+childData.data+str_after_content;
                first_count +=1;
            });
            document.getElementById('post_list').innerHTML = total_post.join('');
            postsRef.on('child_added',function(data){
                second_count +=1;
                if(second_count>first_count){
                    var childData = data.val();
                    total_post[total_post.length] = str_before_username+"<img src="+childData.photoURL+" alt='' class='mr-2 rounded' style='height:32px; width:32px;'>"+mid_str+childData.displayName+"</strong>"+childData.data+str_after_content;
                    document.getElementById('post_list').innerHTML = total_post.join('');
                }
            });
        })
        .catch(e => console.log(e.message))
}

window.onload = function () {
    init();
};