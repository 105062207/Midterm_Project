function initApp() {
    // Login with Email/Password
    var txtEmail = document.getElementById('inputEmail');
    var txtPassword = document.getElementById('inputPassword');
    var btnLogin = document.getElementById('btnLogin');
    var btnGoogle = document.getElementById('btngoogle');
    var btnSignUp = document.getElementById('btnSignUp');

    btnLogin.addEventListener('click',function() {
        var email = txtEmail.value;
        var password = txtPassword.value;
        firebase.auth().signInWithEmailAndPassword(email, password).then(function(){
            document.location.href = "room.html";
        }).catch(function(e){
            create_alert("error",e.message);
            txtEmail.value = "";
            txtPassword.value = "";
        });
    });

    var provider = new firebase.auth.GoogleAuthProvider();

    btnGoogle.addEventListener('click', function () {
        /// TODO 3: Add google login button event
        ///         1. Use popup function to login google
        ///         2. Back to index.html when login success
        ///         3. Show error message by "create_alert"
        firebase.auth().signInWithPopup(provider).then(function (result) {
            var token = result.credential.accessToken;
            var user = result.user;
            console.log(user);
            //document.location.href = "profile.html";
        }).catch(function (error) {
            create_alert("error","111");
        });
    });

    btnSignUp.addEventListener('click', function () {        
        var email = txtEmail.value;
        var password = txtPassword.value;

        /// TODO 4: Add signup button event
        ///         1. Get user input email and password to signup
        ///         2. Show success message by "create_alert" and clean input field
        ///         3. Show error message by "create_alert" and clean input field
        firebase.auth().createUserWithEmailAndPassword(email, password).then(function(){
            firebase.auth().signInWithEmailAndPassword(email, password);
            var userNow = firebase.auth().currentUser;
            //userNow.displayName = userNow.email;
            userNow.updateProfile({
                displayName: userNow.email,
                photoURL : "http://www.hemantlodha.com/wp-content/uploads/2016/03/image1.png",
              });
    
            var newUser = firebase.database().ref('/users/'+userNow.uid);
            console.log(userNow.uid);
            newUser.set({
                UserId: userNow.uid,
                displayName: email,
                photoURL: "http://www.hemantlodha.com/wp-content/uploads/2016/03/image1.png"
            }).then(function(){
                document.location.href = "profile.html";
            });
            
        }).catch(function(e){
            create_alert("error",e.message);
            txtEmail.value = "";
            txtPassword.value = "";
        });
    });
}

// Custom alert
function create_alert(type, message) {
    var alertarea = document.getElementById('custom-alert');
    if (type == "success") {
        str_html = "<div class='alert alert-success alert-dismissible fade show' role='alert'><strong>Success! </strong>" + message + "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
        alertarea.innerHTML = str_html;
    } else if (type == "error") {
        str_html = "<div class='alert alert-danger alert-dismissible fade show' role='alert'><strong>Error! </strong>" + message + "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
        alertarea.innerHTML = str_html;
    }
}

window.onload = function () {
    initApp();
};