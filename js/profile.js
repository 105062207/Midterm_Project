function initprofile()
{
    var user = firebase.auth().currentUser;
    var txtName = document.getElementById('inputName');
    var txtURL = document.getElementById('inputURL');
    var proImg = document.getElementById('pro_img');
    var btnCancel = document.getElementById('cancel');
    var btnSave = document.getElementById('change');
    var user_email = '';
    
    firebase.auth().onAuthStateChanged(function (user) {
      var menu = document.getElementById('dynamic-menu');      
      // Check user login
      if (user) {
          menu.innerHTML = "<span class='dropdown-item'>" + user.email + "</span><span class='dropdown-item' id='profile-btn'>Profile</span><span class='dropdown-item' id='logout-btn'>Logout</span>";
          /// TODO 5: Complete logout button event
          ///         1. Add a listener to logout button 
          ///         2. Show alert when logout success or error (use "then & catch" syntex)
          var btnLogout = document.getElementById("logout-btn");
          btnLogout.addEventListener('click',function(){
              firebase.auth().signOut().then(function(){
                  alert("success");
                  document.location.href="index.html";
              }).catch(function(e){
                  alert("error");
              });
          });
          var btnProfile = document.getElementById("profile-btn");
          btnProfile.addEventListener('click',function(){
              document.location.href = "profile.html";
          });
      } else {
          // It won't show any post if not login
          menu.innerHTML = "<a class='dropdown-item' href='signin.html'>Login</a>";
          //document.getElementById('post_list').innerHTML = "";
      }
  });

  add_btn = document.getElementById('add');
  txt_id = document.getElementById('friendID');
  
  add_btn.addEventListener('click',function(){
    if(txt_id.value!="")
    {
        var userNow = firebase.auth().currentUser;
        var UserId = firebase.database().ref('users');
        var friendExist=false;
        UserId.once("value")
        .then(function(snapshot) {
            var inputID =txt_id.value;
            console.log(txt_id.value);
            friendExist = snapshot.hasChild(inputID);
            console.log(friendExist);
            if(friendExist)
            {
                var friends ="";
                var friendsName="";
                var existed ="";
                firebase.database().ref('users/'+userNow.uid+'/friend').once('value', function(snapshot) {
                    snapshot.forEach(function(childSnapshot) {
                        var friendlist = childSnapshot.val();
                        console.log(friendlist.friendId);
                        if(friendlist.friendId==txt_id.value) existed = true;
                        console.log(existed);
                    });
                    console.log(existed);
                    if(existed == true){
                        alert("You already has the friend!")
                    }else {
                        firebase.database().ref('users/').once('value', function(snapshot) {
                            snapshot.forEach(function(childSnapshot) {
                                friends = childSnapshot.val();
                                if(friends.UserId==txt_id.value) 
                                {
                                    friendsName = friends.displayName;
                                    console.log(friendsName);
                                }
                                var UserFriend = firebase.database().ref('/users/'+userNow.uid+'/friend/'+txt_id.value);
                                UserFriend.set({
                                    friendId: txt_id.value,
                                    friendName:friendsName,
                                    history: ""
                                })
                                var friendFriend = firebase.database().ref('/users/'+txt_id.value+'/friend/'+userNow.uid);
                                friendFriend.set({
                                    friendId:userNow.uid,
                                    friendName:userNow.displayName,
                                    history:""
                                })
                            });
                        });        
                    }    
                });
            }
            else{
                alert("User Not Exist");
            }
        });
    }

});


    firebase.auth().onAuthStateChanged(function(user) {
        if (user) {
          // User is signed in.
          console.log(user.uid);
          document.getElementById("inputName").value = user.displayName;
          document.getElementById('outputID').innerHTML = "<a class='float-left'>User ID :</a><br>"+"<a style='color:red;'>"+user.uid+"</a>";
          var name = user.displayName;
          var userURL = user.photoURL;
          if(userURL==null) userURL = "http://www.hemantlodha.com/wp-content/uploads/2016/03/image1.png"
          document.getElementById('inputURL').value = userURL;
          document.getElementById('pro_img').src = userURL;
        } else {
          // No user is signed in.
          document.getElementById("inputName").value = "NOT FOUND"
        }
      });
      
      btnCancel.addEventListener('click',function(){
        document.location.href = "room.html";
      });
        

      btnSave.addEventListener('click',function(){
        document.getElementById('pro_img').src = document.getElementById('inputURL').value;
        
        var userNow = firebase.auth().currentUser;
        var usersRef = firebase.database().ref('/users');
        usersRef.once('value').then(function(snapshot){
            console.log("WORK");
            snapshot.forEach(function(childSnapshot){
                var childData = childSnapshot.val();
                console.log(childData.UserId);
                if(childData.UserId==userNow.uid)
                {
                    console.log(childData.displayName);
                    childData.displayName = document.getElementById('inputName').value;
                    console.log(childData.displayName);
                    childData.photoURL = document.getElementById('inputURL').value;
                }
            })
        })
        //console.log(usersRef);
        
        userNow.updateProfile({
            displayName: document.getElementById("inputName").value,
            photoURL : document.getElementById("inputURL").value,
          }).then(function() {
            var user = firebase.auth().currentUser;
            /*if (user != null) {
                user.providerData.forEach(function (profile) {
                    console.log("Sign-in provider: " + profile.providerId);
                    console.log("  Provider-specific UID: " + profile.uid);
                    console.log("  Name: " + profile.displayName);
                    console.log("  Email: " + profile.email);
                    console.log("  Photo URL: " + profile.photoURL);
                });
            }*/
            // Update successful.
            //document.location.href = "index.html";
          }).catch(function(error) {
            // An error happened.
            alert("ERROR");
          });
      });
}

window.onload = function(){
    initprofile();
};